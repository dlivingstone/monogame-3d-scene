# README #

MonoGame 3D Scene
https://bitbucket.org/dlivingstone/monogame-3d-scene

### What is this repository for? ###

* A basic 3D scene in MonoGame with Content Pipeline

### How do I get set up? ###

* Download
* Open in Visual Studio
* Run

* You'll need Visual Studio 2013 or later and MonoGame installed

### Contribution guidelines ###

* Report issues on the issue tracker
* No major changes are expected to this project, only minor updates

### Who do I talk to? ###

* Leave issues or comments for me (dlivingstone) via the BitBucket repo
* https://bitbucket.org/dlivingstone/monogame-3d-scene