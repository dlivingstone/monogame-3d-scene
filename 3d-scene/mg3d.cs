﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace _3d_scene
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class mg3d : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //Camera
        Vector3 camTarget;
        Vector3 camPosition;
        Vector3 camForward;
        Matrix projectionMatrix;
        Matrix viewMatrix;

        private SpriteFont font;
        private int currentObject = 0;
        
        //Geometric info
        List<GameObject> objects;

        public mg3d()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            objects = new List<GameObject>();
        }

        protected override void Initialize()
        {
            base.Initialize();

            //Setup Camera
            camForward = new Vector3(0f, 0.0f, 1f);
            camPosition = new Vector3(0f, 0f, -5);

            camTarget = camPosition + camForward;
            projectionMatrix = Matrix.CreatePerspectiveFieldOfView(
                               MathHelper.ToRadians(45f), graphics.
                               GraphicsDevice.Viewport.AspectRatio, 1f, 1000f);
            viewMatrix = Matrix.CreateLookAt(camPosition, camTarget, new Vector3(0f, 1f, 0f));// Y up
            
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            objects.Add(new GameObject() { model = Content.Load<Model>("MonoCube"), position = new Vector3(1, -1, 1) });
            objects.Add(new GameObject() { model = Content.Load<Model>("MonoCube"), position = new Vector3(-5, -1, 1) });
            objects.Add(new GameObject() { model = Content.Load<Model>("MonoCube"), position = new Vector3(0, -1, 25) });
            objects.Add(new GameObject() { model = Content.Load<Model>("MonoCube"), position = new Vector3(6, -1, 6) });
            objects.Add(new GameObject() { model = Content.Load<Model>("MonoCube"), position = new Vector3(0f, -2.5f, 0f), scale = new Vector3(100f, 0.1f, 100f) });

            font = Content.Load<SpriteFont>("Montserrat-Bold");
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back ==
                ButtonState.Pressed || Keyboard.GetState().IsKeyDown(
                Keys.Escape))
                Exit();
            Vector3 oldPos = camPosition;

            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                var rot = Matrix.CreateRotationY(MathHelper.ToRadians(1));
                camForward = Vector3.TransformNormal(camForward, rot);
                camTarget = camPosition + camForward;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                var rot = Matrix.CreateRotationY(MathHelper.ToRadians(-1));
                camForward = Vector3.TransformNormal(camForward, rot);
                camTarget = camPosition + camForward;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                camPosition = camPosition + 0.1f * camForward;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                camPosition = camPosition - 0.1f * camForward;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.W))
            {
                //objects[currentObject].position += new Vector3(0f, 0f, 0.1f);
                objects[currentObject].position += 0.1f * objects[currentObject].rotationMatrix.Forward;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                objects[currentObject].position += 0.1f * objects[currentObject].rotationMatrix.Backward; //new Vector3(0f, 0f, 0.1f);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                objects[currentObject].position += new Vector3(0.1f, 0f, 0.0f);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                objects[currentObject].position -= new Vector3(0.1f, 0f, 0.0f);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Q))
            {
                objects[currentObject].rotationMatrix *= Matrix.CreateRotationY(MathHelper.ToRadians(5));
            }
            if (Keyboard.GetState().IsKeyDown(Keys.E))
            {
                objects[currentObject].rotationMatrix *= Matrix.CreateRotationY(MathHelper.ToRadians(-5));
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Z))
            {
                objects[currentObject].scale -= new Vector3(0.1f, 0.1f, 0.1f);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.X))
            {
                objects[currentObject].scale += new Vector3(0.1f, 0.1f, 0.1f);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.OemOpenBrackets))
            {
                currentObject++;
                if (currentObject >= objects.Count) currentObject = 0;
            } 
            if (Keyboard.GetState().IsKeyDown(Keys.OemCloseBrackets))
            {
                currentObject--;
                if (currentObject < 0) currentObject = objects.Count - 1;
            }

            foreach (GameObject gameObject in objects)
            {
                float distance = (camPosition - gameObject.position).Length();
                if (distance < 2.5f)
                    camPosition = oldPos;
            }
            
            camTarget = camPosition + camForward;
            viewMatrix = Matrix.CreateLookAt(camPosition, camTarget, Vector3.Up);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            GraphicsDevice.DepthStencilState = new DepthStencilState() { DepthBufferEnable = true };

            foreach (GameObject gameObject in objects)
                foreach (ModelMesh mesh in gameObject.model.Meshes)
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        //effect.EnableDefaultLighting();
                        effect.AmbientLightColor = new Vector3(1f, 1f, 1f);
                        effect.View = viewMatrix;
                        effect.World = gameObject.getModelMatrix(); // worldMatrix;
                        effect.Projection = projectionMatrix;
                    }
                    mesh.Draw();
                }


            spriteBatch.Begin();
            spriteBatch.DrawString(font, "Object: " + currentObject, new Vector2(10, 40), Color.Black);
            spriteBatch.DrawString(font, "Position: " + objects[currentObject].position.ToString(), new Vector2(10, 60), Color.Black);
            spriteBatch.DrawString(font, "Scale: " + objects[currentObject].scale.ToString(), new Vector2(10, 80), Color.Black);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }

}
