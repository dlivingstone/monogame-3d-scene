﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace _3d_scene
{
    class GameObject
    {
        private Vector3 _position;
        public Vector3 position
        {
            get { return _position; }
            set { _position = value; }
        }

        private Vector3 _scale;
        public Vector3 scale
        {
            get { return _scale; }
            set { _scale = value; }
        }

        private Matrix _rotationMatrix;
        public Matrix rotationMatrix
        {
            get { return _rotationMatrix; }
            set { _rotationMatrix = value; }
        }

        public Matrix getModelMatrix()
        {
            return Matrix.CreateScale(_scale) * _rotationMatrix * Matrix.CreateWorld(_position, Vector3.Forward, Vector3.Up);
        }

        private string _modelFilename;
        public string modelFilename
        {
            get { return _modelFilename; }
            set { _modelFilename = value; }
        }

        private Model _model;
        public Model model
        {
            get { return _model; }
            set { _model = value; }
        }

        public GameObject()
        {
            _scale = new Vector3(1f, 1f, 1f);
            _rotationMatrix = Matrix.Identity;
            model = null;
        }
    }
}
